###############################################################################################################################################################################################################
############################################################################################################################################################################################################### 
###############################################################################################################################################################################################################
############################################################################################################################################################################################################### 


from __future__ import print_function, division
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
from torchvision import models, transforms
import matplotlib.pyplot as plt
import tkinter 
from tkinter import *
from tkinter import filedialog
from PIL import ImageTk, Image as PILImage

torch.cuda.empty_cache()
window = Tk()

def openfile():   #TKINTER ILE BASIT DOSYA ACMA ARAYUZU
    global filepath
    window.title("Zeytin Kontrol")    
    filepath = filedialog.askopenfilename()
    file =open(filepath,'r')
    print(filepath)
    file.close()
    window.destroy()

button = Button(window, text="Zeytin Yaprağı Kontrol",width=30,height=4,font=("Arial Bold", 14),bg="olive", fg="white", command = openfile)
button1 = Button(window, text="Çıkış",width=30,height=4,font=("Arial Bold", 14),bg="olive", fg="white", command = window.destroy)
button.pack()
button1.pack()
window.mainloop()

plt.ion() 
use_gpu = torch.cuda.is_available()
if use_gpu > 0:
    print("Cuda Kullaniliyor")
else:
    print("CPU kullaniliyor")

torch.cuda.empty_cache()
transform = transforms.Compose([            #[1]
 transforms.Resize(256),                    #[2
 transforms.CenterCrop(224),                #[3]
 transforms.ToTensor(),                     #[4]
 transforms.Normalize(                      #[5]
 mean=[0.485, 0.456, 0.406],                #[6]
 std=[0.229, 0.224, 0.225]                  #[7]
 )])
    

print(filepath)
img = PILImage.open(filepath)
  
img_t = transform(img)

batch_t = torch.unsqueeze(img_t, 0)

batch_t=batch_t.cuda()
vgg16 = models.vgg16(num_classes=3)  

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

vgg16= vgg16.to(device)

for param in vgg16.features.parameters():              #Modelin agirliklarinin dondurulmasi -
  param.requires_grad = False   
    
optimizer = optim.SGD(vgg16.parameters(), lr=0.001, momentum=0.9)


PATH = "vgg16_En_iyi_Model.pt"
vgg16.load_state_dict(torch.load(PATH))

vgg16= vgg16.to(device)
vgg16.eval()
out = vgg16(batch_t)

print(out.shape)

with open('Zeytin_Siniflar.txt') as f:

  classes = [line.strip() for line in f.readlines()]
_, index = torch.max(out, 1)
percentage = torch.nn.functional.softmax(out, dim=1)[0] * 100
print(classes[index[0]], percentage[index[0]].item())
# _, indices = torch.sort(out, descending=True)
# [(classes[idx], percentage[idx].item()) for idx in indices[0][:5]]
print(percentage[index[0]])
plt.figure(figsize=(12, 12))
plt.imshow(img)
pe = percentage[index[0]].item()
ce = classes[index[0]]

plt.title((ce,'%',pe),fontsize=24, fontweight='bold',color='blue')
plt.savefig('Sonuc.jpg')



torch.cuda.empty_cache()





###############################################################################################################################################################################################################
############################################################################################################################################################################################################### 
###############################################################################################################################################################################################################
############################################################################################################################################################################################################### 
