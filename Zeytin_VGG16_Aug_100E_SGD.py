###############################################################################################################################################################################################################
############################################################################################################################################################################################################### 

from __future__ import print_function, division             

import torch                                                      # torch cagirildi
import torch.nn as nn						  # torch neural network atandi	   
import torch.optim as optim       				  # torch optimizerleri atandi	
from torch.optim import lr_scheduler				  # learning rate ayarlama cagirildi
from torch.autograd import Variable     			  # autograd agirliklarin degisimi cagirildi
import numpy as np   						  # numpy matris kutuphanesi cagirildi	
import torchvision  						  # torchvision ile model datasetler ve donusumler cagirildi	
from torchvision import datasets, models, transforms	   	  # Donusumleri cagirmak.
import matplotlib.pyplot as plt					  # matplot pyplot cagirildi
import time							  # zaman degiskeni cagirildi		
import os							  # isletim sistemi kodlari cagirildi
import copy				    			  # kopyalama
import PIL							  # Python resim kutuphanesi	
from PIL import Image 						  # image cagirildi

plt.ion() 							  # interactive plot fonksiyonu
use_gpu = torch.cuda.is_available()				  # CUDA cekirdegi var mi yok mu?
if use_gpu > 0:
    print("Cuda Kullaniliyor")
else:
    print("CPU kullaniliyor") 
    
###############################################################################################################################################################################################################
###############################################################################################################################################################################################################   
Veri_Konumu = '../input/zeytin-aug/Zeytin_224x224_Augmented'
# '../input/zeytin-224x224-augmented-data-set/Zeytin_224x224_Augmented'
# '../input/zeytin/Zeytin'
normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225])        # Normalize ile Imagenet veri setindeki mean ve std degerleri kullanildi
Veri_Donusumleri = {									      # Yuklenen resimlerin on islemeden gecirilmeleri
    'train': transforms.Compose([                  # ardisik transform fonksiyonu
             transforms.Resize(224),               # yeniden boyutlandırma
             transforms.CenterCrop(224),	   # merkezden kesme
             #Rastgele Resim islemeleri Modelin OVERFİT'e gitmesini engellemek icin kullandik.
             transforms.RandomApply(torch.nn.ModuleList([ transforms.Grayscale(3),transforms.RandomCrop(224, padding=None, pad_if_needed=False, fill=0, padding_mode='constant')]), p=0.25)  ,  
             transforms.RandomApply(torch.nn.ModuleList([ transforms.RandomCrop(224, padding=None, pad_if_needed=False, fill=0, padding_mode='constant')]), p=0.90)  ,
             transforms.RandomPerspective(distortion_scale=0.4, p=0.9, fill=0),   # Rastgele filtrelemeler verilen yuzdeye gore sistem rastgele seciyor    
             transforms.RandomHorizontalFlip(),    # rastgele yatay eksende döndürme
#              torchvision.transforms.RandomErasing(p=0.2, scale=(0.1, 0.1), ratio=(0.9, 0.9), value=0, inplace=False), #rastgele bir bolge silme
             transforms.ToTensor(),      # Resim RGB degerlerinin Tensor olarak uc boyutlu matrise cevrilmesi
             normalize
    ]),
    'val': transforms.Compose([
        transforms.Resize(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        normalize
    ]),
    'test': transforms.Compose([
        transforms.Resize(224),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        normalize
    ])
}

Resim_Verisetleri = {
    x: datasets.ImageFolder(
        os.path.join(Veri_Konumu, x), #Veri_Konumu Dosya konumunu os.path ile aldık
        transform=Veri_Donusumleri[x] #Veri dönüşümünü yaptık.
    )
    for x in ['train', 'val', 'test']            # train val ve test etiketli klasorleri aradik
}

Veri_Yukleyiciler = {
    x: torch.utils.data.DataLoader(      
        Resim_Verisetleri[x], batch_size=32,      # verilen paket boyutuna gore resimleri gruplar haline bolduk
        shuffle=True, num_workers=4          	  # resimleri karistirdik
    )
    for x in ['train', 'val', 'test'] 
}

Veri_Yukleciler_Gorsel = {
    x: torch.utils.data.DataLoader(      
        Resim_Verisetleri[x], batch_size=8,      # verilen paket boyutuna gore resimleri gruplar haline bolduk
        shuffle=True, num_workers=4          	  # resimleri karistirdik On gosterim icin 8 resim
    )             
    for x in ['train', 'val', 'test'] 
}

Veriseti_Boyutlar = {x: len(Resim_Verisetleri[x]) for x in ['train', 'val', 'test']}

for x in ['train', 'val', 'test']:
    print("{} Resim  {} bolumunden yuklendi".format(Veriseti_Boyutlar[x], x))     # Veri seti hakkinda temel bilgiler
    
print("Classes: ")
class_names = Resim_Verisetleri['train'].classes      # Sinif adlarini yazdirdik
print(Resim_Verisetleri['train'].classes)

x= datasets.ImageFolder(
        os.path.join(Veri_Konumu, x))

print(x)   #Veri konumunun ozeti
###############################################################################################################################################################################################################
###############################################################################################################################################################################################################
def imshow(inp, title=None):       #Resim gostermek icin tanımlanan fonksiyon
    inp = inp.numpy().transpose((1, 2, 0))   #matrisin devrigi  
    plt.figure(figsize=(25, 25))
    plt.axis('on')
    plt.imshow(inp)
    if title is not None:
        plt.title(title,fontsize=18, fontweight='bold',color='blue')
        plt.pause(0.001)


def show_databatch(inputs, classes):          
    out = torchvision.utils.make_grid(inputs)  #VERI PAKETI GORUNTULENIYOR.
    imshow(out, title=[class_names[x] for x in classes])

# Get a batch of training data
inputs, classes = next(iter(Veri_Yukleciler_Gorsel['train']))
show_databatch(inputs, classes)                             #Yuklenen Resim dosyalarini on gosterim



###############################################################################################################################################################################################################
###############################################################################################################################################################################################################



# Load the pretrained model from pytorch
vgg16 = models.vgg16(pretrained=True)                 #Imagenet Egitim seti ile egitilmis VGG16 Modeli
vgg16.classifier[6].out_features = 3                  # 3 tane siniflandirici secildi
# vgg16.load_state_dict(torch.load('./vgg16_egitilen_model.pt'))    #Disaridan egitilmis model yuklemek istersek bu komutu kullaniriz.
print(vgg16.classifier[6].out_features) # 1000 

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")     #Cuda yoklamasi
    
# Freeze training for all layers
for param in vgg16.features.parameters():              #Modelin agirliklarinin dondurulmasi -
  param.requires_grad = False       #Modelin Agirliklari Donduruluyor.

# Newly created modules have require_grad=True by default
num_features = vgg16.classifier[6].in_features
features = list(vgg16.classifier.children())[:-1] # Remove last layer
features.extend([nn.Linear(num_features, len(class_names))]) # Add our layer with 4 outputs   # Son katmanin Pretrained model ile degistirilmesi
vgg16.classifier = nn.Sequential(*features) # Replace the model classifier        
vgg16= vgg16.to(device)  # VGG16 NN modeli Cuda'ya yuklendi

 
print(vgg16)      #VGG 16 sematigi
# vgg16(img)
print(features)    #OZELLIKLERI YAZDIR

###############################################################################################################################################################################################################
###############################################################################################################################################################################################################

if use_gpu:
    vgg16.cuda() #.cuda() will move everything to the GPU side
criterion = nn.CrossEntropyLoss()                # Kayip degerini bulmak icin kullanilan fonksiyonlardan Birisi Orn MSE MRS 
optimizer = optim.SGD(vgg16.parameters(), lr=0.001, momentum=0.9)   #SGD, ADAM ADAGRAD Optimizatorlar
# optimizer = optim.Adagrad(vgg16.parameters(), lr=0.002, lr_decay=0, weight_decay=0.001)
# optimizer = optim.Adam(vgg16.parameters(), lr=0.001, weight_decay=0)
# optimizer_ft = optim.Adam(vgg16.parameters(), lr=1e-4, betas=(0.9, 0.999),weight_decay=1e-5) L2 Regülarizasyonu

scheduler = optim.lr_scheduler.StepLR(optimizer, step_size="7", gamma=0.1) # Learning Rate icin Dongu programlama
def get_lr(optimizer):
 for param_group in optimizer.param_groups:
    return param_group['lr']            # Ogrenme oraninin ciktisi

###############################################################################################################################################################################################################
###############################################################################################################################################################################################################


# def eval_model(vgg16, criterion):
since = time.time()          # zaman atamasi
avg_loss_t1 = 0    #BASLANGIC ATAMALARI
avg_acc_t1 = 0
loss_test = 0
acc_test = 0

test_batches = len(Veri_Yukleyiciler['test'])
print("Evaluating model")
print('-' * 10)

for i, data in enumerate(Veri_Yukleyiciler['test']):
    if i % 100 == 0:
        print("\rTest batch {}/{}".format(i, test_batches), end='', flush=True)

    vgg16.train(False)        #  Yuklenen Modelin  On degerlendirmesi icin Egitimin dondurulmasi
    vgg16.eval()  	      #  Model Degerlendirme moduna alindi	
    inputs, labels = data

    if use_gpu: #CUDA DEGERLERINE ATAMALAR
        inputs, labels = Variable(inputs.cuda(), volatile=False), Variable(labels.cuda(), volatile=False)  #Cuda ayari
    else:
        inputs, labels = Variable(inputs, volatile=False), Variable(labels, volatile=False)

    outputs = vgg16(inputs)

    _, preds = torch.max(outputs.data, 1)      #Tahminlerin degerlendirilmesi
    loss = criterion(outputs, labels)

    loss_test += loss.data              #kayip degeri
    acc_test += torch.sum(preds == labels.data)       #test degerinin dogrulugu

    del inputs, labels, outputs, preds
    torch.cuda.empty_cache()                    #Grafik rama yuklenme olmamasi icin Cache sifirlaniyor

avg_loss_t1 = loss_test / Veriseti_Boyutlar['test']  #Ortalama kayip
avg_acc_t1 = acc_test / Veriseti_Boyutlar['test']    #Ortalama Dogruluk
avg_acc_t1
elapsed_time = time.time() - since     #Gecen Zaman
print()
print("Test before training")   
print()
print("Evaluation completed in {:.0f}m {:.0f}s".format(elapsed_time // 60, elapsed_time % 60))
print("Avg loss (test): {:.4f}".format(avg_loss_t1))
print("Avg acc (test): {:.4f}".format(avg_acc_t1)) #ON DEGERLENDIRME SONUCLARI YAZDIRMA
print('-' * 10)
    

###############################################################################################################################################################################################################
###############################################################################################################################################################################################################
    
print("Test before training")       #Epochlar oncesi model gosterimi
# eval_model(vgg16, criterion)
# print(avg_acc_t1)  
# print(avg_acc_t1)  

###############################################################################################################################################################################################################
###############################################################################################################################################################################################################

def visualize_model(vgg16, num_images=16):    #Model Gostermek icin kullanilan fonksiyon
    was_training = vgg16.training   #EGITIM MODU
    
    # Set model for evaluation
    vgg16.train(False)     #EGITIM YOK
    vgg16.eval()    #MODEL DEGERLENDIRME MODUNDA
    
    images_so_far = 0

    for i, data in enumerate(Veri_Yukleciler_Gorsel['test']):
        inputs, labels = data   #DATA UZERINDEN INPUT VE LABEL DEGERLERININ ALINMASI
        size = inputs.size()[0] #BOYUT ATAMASI
        
        if use_gpu: #INPUT VE LABEL DEGERLERININ CUDA'YA AKTARILMASI
            inputs, labels = Variable(inputs.cuda(), volatile=False), Variable(labels.cuda(), volatile=False)
        else:
            inputs, labels = Variable(inputs, volatile=False), Variable(labels, volatile=False)
        
        outputs = vgg16(inputs)
        
        _, preds = torch.max(outputs.data, 1)
        predicted_labels = [preds[j] for j in range(inputs.size()[0])]
        
        print("Ground truth:")
        show_databatch(inputs.data.cpu(), labels.data.cpu())   # Olmasi gereken Resim etiketleri ve resimler
        print("Prediction:")
        show_databatch(inputs.data.cpu(), predicted_labels)    # tahmin edilen etiketler ve resimler
        
        del inputs, labels, outputs, preds, predicted_labels
        torch.cuda.empty_cache()
        
        images_so_far += size
        if images_so_far >= num_images:               #resimler istenen sayiya gelince birak
            break
        
    vgg16.train(mode=was_training) # Modeli egitime geri dondurmek


###############################################################################################################################################################################################################
###############################################################################################################################################################################################################

visualize_model(vgg16) #Egitim oncesi gorsel test

###############################################################################################################################################################################################################
###############################################################################################################################################################################################################

def train_model(vgg16, criterion, optimizer,scheduler, num_epochs=70):     #model egitim fonksiyonu 
    since = time.time()
    best_model_wts = copy.deepcopy(vgg16.state_dict())   #en iyi modelin yüklenmesi
    best_acc = 0.0
    
    
    avg_loss = 0               #ilk deger atamaları
    avg_acc = 0
    avg_loss_val = 0
    avg_acc_val = 0
    avg_acc_test = 0
    avg_loss_test = 0
    avg_loss1 = [avg_loss_t1]          #Dizi olarak atamalar(grafik gosterimi icin)
    avg_acc1 = [avg_acc_t1]
    avg_loss_val1 = [avg_loss_t1]
    avg_acc_val1 = [avg_acc_t1]
    avg_acc_test1 = [avg_acc_t1]
    avg_loss_test1 = [avg_loss_t1]
    epoch1 = [0]               #epoch dizisi
    
    train_batches = len(Veri_Yukleyiciler['train'])      #paketler
    val_batches = len(Veri_Yukleyiciler['val'])
    test_batches = len(Veri_Yukleyiciler['test'])
    
    
    for epoch in range(num_epochs):                     #1-100 sirayla epoch for dongusu
        print("Epoch {}/{}".format(epoch, num_epochs))    #{} icine sirasiyla degerler veriliyor
        print('-' * 10)
        epoch=epoch+1
        epoch1.append(epoch)      #epoch1 dizisine [1,2,3,4,5....100] epoch giriliyor
#         print(epoch1)
        loss_train = 0
        loss_val = 0
        acc_train = 0
        acc_val = 0
        loss_test = 0
        acc_test = 0
        
        vgg16.train(True)       #model egitime aliniyor
        
        for i, data in enumerate(Veri_Yukleyiciler['train']):         #train verileri aliniyor
            if i % 100 == 0:
                print("\rTraining batch {}/{}".format(i, train_batches), end='', flush=True)
                
            if i >= train_batches:   #veri seti egitim sonu
                break
                
            inputs, labels = data
            
            if use_gpu:
                inputs, labels = Variable(inputs.cuda(), volatile=False), Variable(labels.cuda(), volatile=False)
            else:
                inputs, labels = Variable(inputs, volatile=False), Variable(labels, volatile=False)
            
            optimizer.zero_grad()    #loss fonksiyonu oncesi gradient sifirlama
            
            outputs = vgg16(inputs) # cikti degerleri vgg girdi degerleri olarak atandi
            
            _, preds = torch.max(outputs.data, 1)   #tensor elementlerinin max degerini alir
            loss = criterion(outputs, labels)     #loss fonksiyonu atanir  nn.CrossEntropyLoss() icine 
            
            loss.backward()     #Geri yayilim algoritmasi
            optimizer.step()    #optimizasyon ilerler
            
            loss_train += loss.data               #loss train icine loss data atiliyor  
            acc_train += torch.sum(preds == labels.data) #tahminler etiketlere esit mi?
            
            del inputs, labels, outputs, preds   #butun degerleri sifirla ve basa don epoch boyunca
            torch.cuda.empty_cache()      #CUDA MEMORY Hatasi almamak icin CACHE temizleniyor.
        
        print()
    
    
        avg_loss = loss_train / Veriseti_Boyutlar['train']     #ortalama accuracy ve loss degerleri bulundu
        avg_acc = acc_train / Veriseti_Boyutlar['train']
        
        vgg16.train(False)         #egitimden cikildi validation ve test modu icin
        vgg16.eval()              #evaluation moduna donuldu
          
        
        
 # TEST VE VALİDATİON ICIN TRAİN İLE AYNI DONGULER GİRİLİYOR FAKAT MODEL EGİTİM MODUNDA DEGİL DEGERLENDİRMEDE!!!!!!!       
 ####################################################################################################################   
        for i, data in enumerate(Veri_Yukleyiciler['val']):
            if i % 100 == 0:
                print("\rValidation batch {}/{}".format(i, val_batches), end='', flush=True)
                
            inputs, labels = data  #girdi ve basliklarin cikarilmasi
            
            if use_gpu:
                inputs, labels = Variable(inputs.cuda(), volatile=False), Variable(labels.cuda(), volatile=False)
            else:
                inputs, labels = Variable(inputs, volatile=False), Variable(labels, volatile=False)
            
            optimizer.zero_grad()
            
            outputs = vgg16(inputs)
            
            _, preds = torch.max(outputs.data, 1)
            loss = criterion(outputs, labels)
            
            loss_val += loss.data #Validation kaybi
            acc_val += torch.sum(preds == labels.data) #Validation Dogrulugu
            
            del inputs, labels, outputs, preds
            torch.cuda.empty_cache() #CUDA MEMORY Hatasi almamak icin CACHE temizleniyor.
            
        for i, data in enumerate(Veri_Yukleyiciler['test']):
            
            if i % 100 == 0:
                print("\rTest batch {}/{}".format(i, test_batches), end='', flush=True)

                
            inputs, labels = data

            if use_gpu:
                inputs, labels = Variable(inputs.cuda(), volatile=False), Variable(labels.cuda(), volatile=False)
            else:
                inputs, labels = Variable(inputs, volatile=False), Variable(labels, volatile=False)

            outputs = vgg16(inputs)

            _, preds = torch.max(outputs.data, 1)
            loss = criterion(outputs, labels)

            loss_test += loss.data   #Test kaybi
            acc_test += torch.sum(preds == labels.data)  #Test dogrulugu

            del inputs, labels, outputs, preds
            torch.cuda.empty_cache() #CUDA MEMORY Hatasi almamak icin CACHE temizleniyor.
                
        avg_loss_val = loss_val / Veriseti_Boyutlar['val']    #VAL VE TEST Degerleri ortalamya aliniyor.
        avg_acc_val = acc_val / Veriseti_Boyutlar['val']        
        avg_acc_test = acc_test / Veriseti_Boyutlar['test']
        avg_loss_test = loss_test / Veriseti_Boyutlar['test']
       
# Her dongu sonunda bulunan ortalama dogruluk ve kayip degerleri dizilere aktariliyor..        
        avg_loss1.append(avg_loss) 
        avg_acc1.append(avg_acc)
        avg_acc_val1.append(avg_acc_val)
        avg_loss_val1.append(avg_loss_val)
        avg_acc_test1.append(avg_acc_test)
        avg_loss_test1.append(avg_loss_test)
        
        if avg_acc_val > best_acc:
            best_acc = avg_acc_val         #En iyi Validasyon sonucu aliniyor
            best_model_wts = copy.deepcopy(vgg16.state_dict())  #en iyi model yukleniyor.
        # Modelin her epoch dongusu Console ekranina Cikti olarak gosteriliyor.            
        print()  
        print("---------------------------------------------------------------------")
        print("---------------------------------------------------------------------")
        print("<<< {}.TUR(EPOCH) SONUCLARİ >>>             ".format(epoch))
        print("Ogrenme Orani                        : ",get_lr(optimizer))
        print("Ortalama kayip (train)               : {:.4f}".format(avg_loss)) #Virgulden sonra 4 hane
        print("Ortalama dogru bulma (train)         : {:.4f}".format(avg_acc))
        print("---------------------------------------------------------------------")
        print("Ortalama kayip (val)                 : {:.4f}".format(avg_loss_val))
        print("Ortalama dogru bulma (val)           : {:.4f}".format(avg_acc_val))
        print("---------------------------------------------------------------------")
        print("TEST SETİ KAYBI                      : {:.4f}".format(avg_loss_test))
        print("TEST SETİNİN  DOGRU BULMA YUZDESİ    : {:.4f}".format(avg_acc_test))
        print("---------------------------------------------------------------------")
        print("---------------------------------------------------------------------")
#Alttaki satirlari yorumdan cikararak her dongudeki test degerinin grafiksel degisimini gosterebiliriz.       
#         plt.plot(epoch1,avg_acc_test1, color='red', label='Test Seti ortalaması')
#         plt.grid(b=None, which='major', axis='both')
#         plt.axis([0, epoch, 0, 1])
#         plt.show()
    #Egitim degerlendirmesi zaman, en dogru vb.      
    elapsed_time = time.time() - since   #Gecen zaman
    print()
    print("Egitim {:.0f}m {:.0f}s 'içinde tamamlandi.'".format(elapsed_time // 60, elapsed_time % 60))
    print("En dogru yuzde: {:.4f}".format(best_acc))
    
    vgg16.load_state_dict(best_model_wts)  #En iyi modelin yuklenmesi
    
#EGITIM SONUCUNDA TEST,VALİD VE ACCURACY DEGERLERİNİN EPOCH TABANLI DEGISIMINI GOSTEREN GRAFİKLER
#GRAFIKLERDE BASLANGIC DEGERLERI ICIN PRETRAINED MODELIN ON DEGERLENDIRMESI ALINDI.
#HAZIR MODELDE 20.EPOCHTAN SONRA LOSS DEGERLERININ YUKSELDIGI GORULDU.
    plt.figure(figsize=(12, 10))
    plt.plot(epoch1,avg_acc1, color='green', label=' Egitim Seti  dogruluk') #degerlerin dizileri
    plt.plot(epoch1,avg_acc_val1, color='blue', label='validasyon ortalaması')
    plt.plot(epoch1,avg_acc_test1, color='red', label='Test Seti ortalaması')
    plt.xlabel('Epoch Sayisi')  #x ve y eksen etiketleri
    plt.ylabel('Dogruluk orani')
    plt.title('Train,Test,Val Dogrulugu') #grafik basliklari
    plt.annotate('Pretrain Imagenet Dogruluk Degeri',fontsize=14, fontweight='bold',color='green',
            xy=(0, avg_acc_t1),  # theta, radius
            xytext=(num_epochs/6, avg_acc_t1*0.8 ),    # fraction, fraction
            textcoords='data',
            arrowprops=dict(facecolor='black', shrink=0.015),  #ok ile baslangic degerlerinin gosterilmesi
            horizontalalignment='left',
            verticalalignment='bottom')
#     plt.annotate('Egitimsiz Pretrained Dogruluk Degeri', xy=(0, avg_acc_t1), xytext=(3, 4),arrowprops=dict(facecolor='black', shrink=0.05)
    plt.axis([0, num_epochs, 0, 1])
    plt.legend()
    plt.grid(b=None, which='major', axis='both')  #izgara at
    plt.savefig('accuracy.png')  #figuru kaydet.
    plt.show()  #ACC PLOTU

    
    plt.figure(figsize=(12, 10))
    plt.plot(epoch1,(avg_loss1), color='green', label='Egitim Seti kaybı')
    plt.plot(epoch1,avg_loss_val1, color='blue', label='validasyon kaybi')
    plt.plot(epoch1,avg_loss_test1, color='red', label='Test Seti kaybi')
    plt.xlabel('Epoch Sayisi')
    plt.ylabel('Kayip Orani')
    plt.title('Train,Test,Val Kayip Oranlari') #
    plt.annotate('Pretrain Imagenet Kayip Degeri',fontsize=14, fontweight='bold',color='red',
            xy=(0, avg_loss_t1),  # theta, radius
            xytext=(num_epochs/6, (avg_loss_t1.cpu().detach().numpy())*0.92),    # fraction, fraction
            textcoords='data',
            arrowprops=dict(facecolor='black', shrink=0.015),
            horizontalalignment='left',
            verticalalignment='bottom')
#     plt.annotate('Egitimsiz Pretrained Loss Degeri', xy=(0, avg_loss_t1), xytext=(3, 4),arrowprops=dict(facecolor='black', shrink=0.05)
    plt.axis([0, num_epochs, 0, avg_loss_t1.cpu().detach().numpy()])
    plt.legend()
    plt.grid(b=None, which='major', axis='both')
    plt.savefig('loss.png')
    plt.show()  #LOSS PLOTU
####################################################################################################################################################   
#CONFUSION MATRISININ KODLARI    

#CONFUSION MATRISINI OLUSTURMA
    Sinif_Sayisi = 3

    confusion_matrix = torch.zeros(Sinif_Sayisi, Sinif_Sayisi) #3x3 conf matrisi
    with torch.no_grad():
        for i, (inputs, classes) in enumerate(Veri_Yukleyiciler['test']): 
            inputs = inputs.to(device)
            classes = classes.to(device) #cuda atamasi
            outputs = vgg16(inputs)
            _, preds = torch.max(outputs, 1)  #tahmin
            for t, p in zip(classes.view(-1), preds.view(-1)):
                    confusion_matrix[t.long(), p.long()] += 1
    confusion_matrix = confusion_matrix.cpu().detach().numpy()  #ifadeyi tensor disina almak.    
    
#CONFUSION MATRISINI GRAFIK UZERINDE GOSTERME

    fig, ax = plt.subplots(figsize=(18, 18))
    im = ax.imshow(confusion_matrix)
    
    ax.set_xticks(np.arange(len(class_names))) # x ve y degerleri class isimlerinden cekiliyor
    ax.set_yticks(np.arange(len(class_names)))  
    
    ax.set_xticklabels(class_names)
    ax.set_yticklabels(class_names)  
    
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor",fontsize=40,fontweight='bold')
    
    plt.setp(ax.get_yticklabels(),fontsize=40,fontweight='bold')
    
    for i in range(len(class_names)):
        for j in range(len(class_names)):
            text = ax.text(j, i, confusion_matrix[i, j], #confusion matrix x ve y degerleri grafige atanmasi
                       ha="center", va="center", color="black",fontsize=50,fontweight='bold')
    print(confusion_matrix)
    ax.set_title("Zeytin Hastaliklari Confusion Matrisi.",fontsize=40,color="r",fontweight='bold')
    fig.tight_layout()   #SIKI DIZILIM
    plt.show()
##################################################################################################################################################
    return vgg16 #VGG16 DEGERİNİ FONKSIYON DISINA ALDIK.


#//////////////////////////// VGG16 MODELININ PYTHON DILINE AKTARIMI \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#//////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


# vgg16 = nn.Sequential(
#     nn.Conv2d(3, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
#     nn.Conv2d(64, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
#     nn.Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
#     nn.Conv2d(256, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
#     nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
#     nn.BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
#     nn.ReLU(inplace=True),
#     nn.MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
#     nn.AdaptiveAvgPool2d(output_size=(7, 7)),
    
#     nn.Linear(in_features=25088, out_features=4096, bias=True),
#     nn.ReLU(inplace=True),
#     nn.Dropout(p=0.5, inplace=False),
#     nn.Linear(in_features=4096, out_features=4096, bias=True),
#     nn.ReLU(inplace=True),
#     nn.Dropout(p=0.5, inplace=False),
#     nn.Linear(in_features=4096, out_features=3, bias=True))

#//////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
#//////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

###############################################################################################################################################################################################################
###############################################################################################################################################################################################################
  

vgg16 = train_model(vgg16, criterion, optimizer,scheduler, num_epochs=75)# EGITIM FONKSIYONUNUN CAGIRILMASI VE DEGERLERININ ATANMASI
torch.save(vgg16.state_dict(), '../working/vgg16_En_iyi_Model.pt')    # EGITIM SONUNDA EN IYI MODEL KAYDEDILIR !!!!!
#NOT: Kaydedılen model VGG16'nin mimarisi yuzunden 450-500mb civarinda olacaktir boyutlar biraz buyuk.



###############################################################################################################################################################################################################
###############################################################################################################################################################################################################

visualize_model(vgg16, num_images=32)  #Egitilen modelin 8x4 Ground Truth ve Tahmin olarak gosterilmesi

###############################################################################################################################################################################################################
###############################################################################################################################################################################################################
